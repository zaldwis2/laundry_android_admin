package removed.developer.adminlaundry.DataClass;

public class DeliveryOrders {

    private String nameCustomer;
    private String locationCustomer;
    private String phoneCustomer;
    private String weightOrder;
    private String namePackage;
    private String nameService;

    public DeliveryOrders(String nameCustomer, String locationCustomer, String phoneCustomer, String weightOrder, String namePackage, String nameService) {
        this.nameCustomer = nameCustomer;
        this.locationCustomer = locationCustomer;
        this.phoneCustomer = phoneCustomer;
        this.weightOrder = weightOrder;
        this.namePackage = namePackage;
        this.nameService = nameService;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getLocationCustomer() {
        return locationCustomer;
    }

    public void setLocationCustomer(String locationCustomer) {
        this.locationCustomer = locationCustomer;
    }

    public String getPhoneCustomer() {
        return phoneCustomer;
    }

    public void setPhoneCustomer(String phoneCustomer) {
        this.phoneCustomer = phoneCustomer;
    }

    public String getWeightOrder() {
        return weightOrder;
    }

    public void setWeightOrder(String weightOrder) {
        this.weightOrder = weightOrder;
    }

    public String getNamePackage() {
        return namePackage;
    }

    public void setNamePackage(String namePackage) {
        this.namePackage = namePackage;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }
}
