package removed.developer.adminlaundry.DataClass;

public class ProgressOrders {

    private String nameCustomer;
    private String locationCustomer;
    private String phoneCustomer;
    private String weightOrder;
    private String namePackage;
    private String nameService;
    private String dateFinish;
    private String amountPrice;
    private String paymentStatus;

    public ProgressOrders(String nameCustomer, String locationCustomer, String phoneCustomer, String weightOrder, String namePackage, String nameService, String dateFinish, String amountPrice, String paymentStatus) {
        this.nameCustomer = nameCustomer;
        this.locationCustomer = locationCustomer;
        this.phoneCustomer = phoneCustomer;
        this.weightOrder = weightOrder;
        this.namePackage = namePackage;
        this.nameService = nameService;
        this.dateFinish = dateFinish;
        this.amountPrice = amountPrice;
        this.paymentStatus = paymentStatus;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getLocationCustomer() {
        return locationCustomer;
    }

    public void setLocationCustomer(String locationCustomer) {
        this.locationCustomer = locationCustomer;
    }

    public String getPhoneCustomer() {
        return phoneCustomer;
    }

    public void setPhoneCustomer(String phoneCustomer) {
        this.phoneCustomer = phoneCustomer;
    }

    public String getWeightOrder() {
        return weightOrder;
    }

    public void setWeightOrder(String weightOrder) {
        this.weightOrder = weightOrder;
    }

    public String getNamePackage() {
        return namePackage;
    }

    public void setNamePackage(String namePackage) {
        this.namePackage = namePackage;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getAmountPrice() {
        return amountPrice;
    }

    public void setAmountPrice(String amountPrice) {
        this.amountPrice = amountPrice;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
