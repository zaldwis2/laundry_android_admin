package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

import removed.developer.adminlaundry.Adapter.AdapterPickupOrder;
import removed.developer.adminlaundry.DataClass.Pickups;
import removed.developer.adminlaundry.databinding.ActivityInputWeightOrderBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityInputWeightOrder extends AppCompatActivity implements View.OnClickListener {

    ActivityInputWeightOrderBinding binding;
    private final int PACKET_KILAT = 1;
    private final int PACKET_BIASA = 2;
    private final int CUCI_GOSOK = 1;
    private final int CUCI_LIPAT = 2;
    private final int PRICE_KILAT = 5000;
    private final int PRICE_BIASA = 0;
    private final int PRICE_GOSOK = 5000;
    private final int PRICE_LIPAT = 0;
    private final int PRICE_UNIT = 7000;
    private final int PRICE_ONGKIR = 5000;
    String weight, totalPriceG;
    Integer serviceID, packetID,orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityInputWeightOrderBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonCount.setOnClickListener(this);
        binding.buttonProcess.setOnClickListener(this);

        serviceID = getIntent().getIntExtra("service_id",0);
        packetID = getIntent().getIntExtra("packet_id",0);
        orderId = getIntent().getIntExtra("order_id",0);

        if (serviceID.equals(1)){
            binding.textViewServices.setText("Layanan : Cuci + Gosok");
        } else {
            binding.textViewServices.setText("Layanan : Cuci + Lipat");
        }

        if (packetID.equals(1)){
            binding.textViewPackage.setText("Paket : Kilat");
        } else {
            binding.textViewPackage.setText("Paket : Biasa");
        }

        binding.toolbar.setTitle("Masukkan Berat");
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.editTextWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (binding.editTextWeight.getText().toString().trim().equals("")) {
                    binding.textViewAmountPrice.setText("");
                }
                if (binding.textViewAmountPrice.getText().toString().trim().isEmpty()) {
                    binding.buttonProcess.setEnabled(false);
                } else {
                    binding.buttonProcess.setEnabled(true);
                }
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonCount:
                weight = binding.editTextWeight.getText().toString().trim();
                if (!weight.isEmpty()) {
                    calculatePrice(Integer.valueOf(weight));
                } else {
                    Toast.makeText(this, "masukkin beratnya dulu min", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.buttonProcess:
                updateOrder(orderId,totalPriceG,weight);
                break;
        }
    }

    private void calculatePrice(Integer weight) {
        Integer totalPrice = 0;

        Locale localeID = new Locale("in", "ID");
        final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        if (packetID == PACKET_KILAT){
            totalPrice += PRICE_KILAT;
        }

        if (serviceID == CUCI_GOSOK){
            totalPrice += PRICE_GOSOK;
        }

        totalPrice += (weight * PRICE_UNIT);
        totalPrice += PRICE_ONGKIR;

        binding.textViewAmountPrice.setText(formatRupiah.format((double) totalPrice));
        binding.buttonProcess.setEnabled(true);

        totalPriceG = String.valueOf(totalPrice);
    }

    private void updateOrder(Integer id, String price, String weight){
        binding.buttonProcess.setEnabled(false);
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.updateOrder(
                id,
                "",
                "1",
                "",
                "",
                weight,
                price
        );
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    onBackPressed();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                    binding.buttonProcess.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                binding.buttonProcess.setEnabled(true);
            }
        });
    }
}