package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import removed.developer.adminlaundry.databinding.ActivityRegisterBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener{

    ActivityRegisterBinding binding;
    private String strEmail, strName, strPhone, strBranch, strPassword, strConfirmPassword;
    private boolean isEmailNotEmpty, isNameNotEmpty, isPhoneNotEmpty, isBranchNotEmpty, isPasswordNotEmpty, isConfirmPassNotEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonRegister.setOnClickListener(this);
        binding.textViewLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        strEmail = binding.editTextEmail.getText().toString().trim();
        strName = binding.editTextName.getText().toString().trim();
        strPhone = binding.editTextPhone.getText().toString().trim();
        strBranch = binding.editTextBranch.getText().toString().trim();
        strPassword = binding.editTextPassword.getText().toString().trim();
        strConfirmPassword = binding.editTextConfirmPassword.getText().toString().trim();

        switch (view.getId()){
            case R.id.buttonRegister:
                if (strEmail.equals("")){
                    isEmailNotEmpty = false;
                    binding.editTextEmail.setError("e-mail kosong");
                } else if (!isValidEmailId(strEmail)){
                    isEmailNotEmpty = false;
                    binding.editTextEmail.setError("e-mail tidak sesuai");
                } else {
                    isEmailNotEmpty = true;
                    strEmail = binding.editTextEmail.getText().toString().trim();
                }

                if (strName.equals("")){
                    isNameNotEmpty = false;
                    binding.editTextName.setError("nama kosong");
                } else {
                    isNameNotEmpty = true;
                    strName = binding.editTextName.getText().toString().trim();
                }

                if (strPhone.equals("")){
                    isPhoneNotEmpty = false;
                    binding.editTextPhone.setError("nomor hp kosong");
                } else if (!isValidMobile(strPhone)){
                    isPhoneNotEmpty = false;
                    binding.editTextPhone.setError("nomor hp tidak sesuai");
                } else {
                    isPhoneNotEmpty = true;
                    strPhone = binding.editTextPhone.getText().toString().trim();
                }

                if (strBranch.equals("")){
                    isBranchNotEmpty = false;
                    binding.editTextBranch.setError("cabang kosong");
                } else {
                    isBranchNotEmpty = true;
                    strBranch = binding.editTextBranch.getText().toString().trim();
                }

                if (strPassword.equals("")){
                    isPasswordNotEmpty = false;
                    binding.editTextPassword.setError("password kosong");
                } else {
                    isPasswordNotEmpty = true;
                    strPassword = binding.editTextPassword.getText().toString().trim();
                }

                if (strConfirmPassword.equals("")){
                    isConfirmPassNotEmpty = false;
                    binding.editTextConfirmPassword.setError("konfirmasi password kosong");
                } else if (!strConfirmPassword.equals(strPassword)){
                    isConfirmPassNotEmpty = false;
                    binding.editTextConfirmPassword.setError("konfirmasi password tidak sesuai");
                } else {
                    isConfirmPassNotEmpty = true;
                    strConfirmPassword = binding.editTextConfirmPassword.getText().toString().trim();
                }

                if ((isEmailNotEmpty==true) && (isNameNotEmpty==true) && (isPhoneNotEmpty==true) && (isBranchNotEmpty==true) && (isPasswordNotEmpty==true) && (isConfirmPassNotEmpty==true)){
                    registerAPI();
                }
                break;

            case R.id.textViewLogin:
                onBackPressed();
                break;
        }
    }

    private void registerAPI() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.register(
                binding.editTextEmail.getText().toString(),
                binding.editTextName.getText().toString(),
                binding.editTextPhone.getText().toString(),
                binding.editTextBranch.getText().toString(),
                binding.editTextConfirmPassword.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ActivityRegister.this, ActivityLogin.class);
                    startActivity(intent);
                    finishAffinity();
                }else{
                    Toast.makeText(getApplicationContext(),"Email Already Exist",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidEmailId(String email){
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 9 && phone.length() <= 13;
        }
        return false;
    }
}