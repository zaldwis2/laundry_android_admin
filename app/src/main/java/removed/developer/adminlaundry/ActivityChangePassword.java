package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import removed.developer.adminlaundry.databinding.ActivityChangePasswordBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChangePassword extends AppCompatActivity {

    ActivityChangePasswordBinding binding;
    private String strOldPassword, strNewPassword, strConfirmPassword;
    private boolean oldPassFill, newPassFill, confirmPassFill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOldPassword = binding.editTextOldPassword.getText().toString().trim();
                strNewPassword = binding.editTextNewPassword.getText().toString().trim();
                strConfirmPassword = binding.editTextConfirmNewPassword.getText().toString().trim();

                if (strOldPassword.equals("")){
                    oldPassFill = false;
                    binding.editTextOldPassword.setError("password kosong");
                } else {
                    oldPassFill = true;
                    strOldPassword = binding.editTextOldPassword.getText().toString().trim();
                }

                if (strNewPassword.equals("")){
                    newPassFill = false;
                    binding.editTextNewPassword.setError("password baru kosong");
                } else if (strNewPassword.equals(strOldPassword)){
                    newPassFill = false;
                    binding.editTextNewPassword.setError("password baru tidak boleh sama dengan yang lama");
                } else {
                    newPassFill = true;
                    strNewPassword = binding.editTextNewPassword.getText().toString().trim();
                }

                if (strConfirmPassword.equals("")){
                    confirmPassFill = false;
                    binding.editTextConfirmNewPassword.setError("konfirmasi password kosong");
                } else if (!strConfirmPassword.equals(strNewPassword)){
                    confirmPassFill = false;
                    binding.editTextConfirmNewPassword.setError("konfirmasi password tidak sesuai");
                } else {
                    confirmPassFill = true;
                    strConfirmPassword = binding.editTextConfirmNewPassword.getText().toString().trim();
                }

                if ((oldPassFill==true) && (newPassFill==true) && (confirmPassFill==true)){
                    updateDataAPI(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId());
                }
            }
        });
    }

    private void updateDataAPI(String id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.changePasswordAdmin(
                id,
                strOldPassword,
                strConfirmPassword);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage()+", silakan gunakan password baru untuk login", Toast.LENGTH_LONG).show();
                    binding.editTextOldPassword.setText("");
                    binding.editTextNewPassword.setText("");
                    binding.editTextConfirmNewPassword.setText("");
                } else {
                    Toast.makeText(getApplicationContext(), "Failed Retrieve Data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}