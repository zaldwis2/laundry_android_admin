package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import removed.developer.adminlaundry.Adapter.AdapterDeliveryOrder;
import removed.developer.adminlaundry.Adapter.AdapterIncomingOrder;
import removed.developer.adminlaundry.Adapter.AdapterPickupOrder;
import removed.developer.adminlaundry.DataClass.Pickups;
import removed.developer.adminlaundry.databinding.ActivityPickupOrderBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPickupOrder extends AppCompatActivity {

    ActivityPickupOrderBinding binding;

    private AdapterPickupOrder adapterPickupOrder;
    private ArrayList<Pickups> pickupsArrayList;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPickupOrderBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.toolbar.setTitle("Orderan yang dijemput");
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        swipeRefresh();
    }

    private void swipeRefresh(){
        binding.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getData();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.refresh.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    private void getData() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getPickupOrders(SharedPreferenceManager.getInstance(getApplicationContext()).getLaundryId());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataPickupOrder() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityPickupOrder.this);
                        binding.recyclerView.setLayoutManager(linearLayoutManager);
                        adapterPickupOrder = new AdapterPickupOrder(response.body().getDataPickupOrder(), ActivityPickupOrder.this);
                        binding.recyclerView.setAdapter(adapterPickupOrder);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed Retrieve Data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}