package removed.developer.adminlaundry.DataClass;

public class IncomingOrders {
    private String datePickup;
    private String nameCustomer;
    private String phoneCustomer;
    private String pickupLocation;
    private String service;
    private String packageName;

    public IncomingOrders(String datePickup, String nameCustomer, String phoneCustomer, String pickupLocation, String service, String packageName) {
        this.datePickup = datePickup;
        this.nameCustomer = nameCustomer;
        this.phoneCustomer = phoneCustomer;
        this.pickupLocation = pickupLocation;
        this.service = service;
        this.packageName = packageName;
    }

    public String getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(String datePickup) {
        this.datePickup = datePickup;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getPhoneCustomer() {
        return phoneCustomer;
    }

    public void setPhoneCustomer(String phoneCustomer) {
        this.phoneCustomer = phoneCustomer;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
