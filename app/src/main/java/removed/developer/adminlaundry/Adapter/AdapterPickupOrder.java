package removed.developer.adminlaundry.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.adminlaundry.ActivityInputWeightOrder;
import removed.developer.adminlaundry.CustomView.MyButton;
import removed.developer.adminlaundry.DataClass.Pickups;
import removed.developer.adminlaundry.R;
import removed.developer.adminlaundry.models.DataOrder;

public class AdapterPickupOrder extends RecyclerView.Adapter<AdapterPickupOrder.ViewHolder> {

    private List<DataOrder> pickupsArrayList;
    private Context context;

    public AdapterPickupOrder(List<DataOrder> pickupsArrayList, Context context) {
        this.pickupsArrayList = pickupsArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterPickupOrder.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pickup_order, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterPickupOrder.ViewHolder holder, int position) {
        final DataOrder pickups = pickupsArrayList.get(position);

        holder.textViewNameCustomer.setText(pickups.getCustomerName());
        holder.textViewPhone.setText(pickups.getCustomerPhone());
        holder.textViewLocationCustomer.setText(pickups.getLocationName());
        holder.textViewService.setText(pickups.getServiceName());
        holder.textViewPackage.setText(pickups.getPacketName());
        holder.buttonArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityInputWeightOrder.class);
                intent.putExtra("service_id",pickups.getServiceId());
                intent.putExtra("packet_id",pickups.getPacketId());
                intent.putExtra("order_id",pickups.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pickupsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewNameCustomer, textViewPhone, textViewLocationCustomer, textViewService, textViewPackage;
        private MyButton buttonArrived;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNameCustomer = itemView.findViewById(R.id.textViewNameCustomer);
            textViewPhone = itemView.findViewById(R.id.textViewPhone);
            textViewLocationCustomer = itemView.findViewById(R.id.textViewLocationCustomer);
            textViewService = itemView.findViewById(R.id.textViewService);
            textViewPackage = itemView.findViewById(R.id.textViewPackage);
            buttonArrived = itemView.findViewById(R.id.buttonArrived);
        }
    }
}
