package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import removed.developer.adminlaundry.databinding.ActivityChangeProfileBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChangeProfile extends AppCompatActivity {

    ActivityChangeProfileBinding binding;
    private String strEmail, strName, strPhone, strBranch;
    private boolean emailFill, nameFill, phoneFill, branchFill = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangeProfileBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        dataAdminAPI(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId());

        binding.buttonChangeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strEmail = binding.editTextEmail.getText().toString().trim();
                strName = binding.editTextName.getText().toString().trim();
                strPhone = binding.editTextPhone.getText().toString().trim();
                strBranch = binding.editTextBranch.getText().toString().trim();

                if (strEmail.equals("")) {
                    emailFill = false;
                    binding.editTextEmail.setError("e-mail kosong");
                } else if (!isValidEmailId(strEmail)) {
                    emailFill = false;
                    binding.editTextEmail.setError("e-mail tidak sesuai");
                } else {
                    emailFill = true;
                    strEmail = binding.editTextEmail.getText().toString().trim();
                }

                if (strName.equals("")) {
                    nameFill = false;
                    binding.editTextName.setError("nama kosong");
                } else {
                    nameFill = true;
                    strName = binding.editTextName.getText().toString().trim();
                }

                if (strPhone.equals("")) {
                    phoneFill = false;
                    binding.editTextPhone.setError("nomor hp kosong");
                } else if (!isValidMobile(strPhone)) {
                    phoneFill = false;
                    binding.editTextPhone.setError("nomor hp tidak sesuai");
                } else {
                    phoneFill = true;
                    strPhone = binding.editTextPhone.getText().toString().trim();
                }

                if (strBranch.equals("")) {
                    branchFill = false;
                    binding.editTextBranch.setError("cabang kosong");
                } else {
                    branchFill = true;
                    strBranch = binding.editTextBranch.getText().toString().trim();
                }

                if ((emailFill==true) && (nameFill==true) && (phoneFill==true) && (branchFill==true)){
                    updateDataAPI(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId());
                }
            }
        });
    }

    private void dataAdminAPI(String id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getDataAdmin(id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    binding.editTextEmail.setText(response.body().getDataAdmin().getEmail());
                    binding.editTextName.setText(response.body().getDataAdmin().getName());
                    binding.editTextPhone.setText(response.body().getDataAdmin().getPhone());
                    binding.editTextBranch.setText(response.body().getDataAdmin().getLaundryName());
                } else {
                    Toast.makeText(getApplicationContext(), "Failed Retrieve Data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateDataAPI(String id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.updateAdminData(
                id,
                binding.editTextName.getText().toString(),
                binding.editTextEmail.getText().toString(),
                binding.editTextPhone.getText().toString(),
                SharedPreferenceManager.getInstance(getApplicationContext()).getLaundryId(),
                binding.editTextBranch.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    Intent intentDashboard = new Intent(ActivityChangeProfile.this, ActivityDashboard.class);
                    startActivity(intentDashboard);
                    finishAffinity();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed Retrieve Data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidEmailId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 9 && phone.length() <= 13;
        }
        return false;
    }


}