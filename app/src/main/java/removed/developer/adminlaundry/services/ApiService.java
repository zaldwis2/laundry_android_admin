package removed.developer.adminlaundry.services;

import removed.developer.adminlaundry.models.ResponseData;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("registerAdmin")
    Call<ResponseData> register(
            @Field("email") String email,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("laundry_name") String laundry_name,
            @Field("password") String password
    );

    @GET("loginAdmin")
    Call<ResponseData> login(
            @Query("email") String email,
            @Query("password") String password
    );

    @GET("getBalanceAdmin")
    Call<ResponseData> getBalance(
            @Query("id") String id
    );

    @GET("getDataAdmin")
    Call<ResponseData> getDataAdmin(
            @Query("id") String id
    );

    @FormUrlEncoded
    @PUT("updateAdminData")
    Call<ResponseData> updateAdminData(
            @Field("id") String id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("laundry_id") String laundry_id,
            @Field("laundry_name") String laundry_name
    );

    @FormUrlEncoded
    @PUT("changePasswordAdmin")
    Call<ResponseData> changePasswordAdmin(
            @Field("id") String id,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password
    );

    @FormUrlEncoded
    @PUT("updateOrder")
    Call<ResponseData> updateOrder(
            @Field("id") Integer id,
            @Field("is_processed") String is_processed,
            @Field("is_picked") String is_picked,
            @Field("is_accepted") String is_accepted,
            @Field("is_delivered") String is_delivered,
            @Field("weight") String weight,
            @Field("price") String price
    );

    @GET("getIncomingOrders")
    Call<ResponseData> getIncomingOrders(
            @Query("laundry_id") String laundry_id
    );

    @GET("getPickupOrders")
    Call<ResponseData> getPickupOrders(
            @Query("laundry_id") String laundry_id
    );

    @GET("getProcessOrder")
    Call<ResponseData> getProcessOrder(
            @Query("laundry_id") String laundry_id
    );

    @GET("getDeliverOrder")
    Call<ResponseData> getDeliverOrder(
            @Query("laundry_id") String laundry_id
    );
}
