package removed.developer.adminlaundry.DataClass;

public class Pickups {

    private String nameCustomer;
    private String phoneCustomer;
    private String locationCustomer;
    private String nameService;
    private String namePackage;

    public Pickups(String nameCustomer, String phoneCustomer, String locationCustomer, String nameService, String namePackage) {
        this.nameCustomer = nameCustomer;
        this.phoneCustomer = phoneCustomer;
        this.locationCustomer = locationCustomer;
        this.nameService = nameService;
        this.namePackage = namePackage;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getPhoneCustomer() {
        return phoneCustomer;
    }

    public void setPhoneCustomer(String phoneCustomer) {
        this.phoneCustomer = phoneCustomer;
    }

    public String getLocationCustomer() {
        return locationCustomer;
    }

    public void setLocationCustomer(String locationCustomer) {
        this.locationCustomer = locationCustomer;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }

    public String getNamePackage() {
        return namePackage;
    }

    public void setNamePackage(String namePackage) {
        this.namePackage = namePackage;
    }
}
