package removed.developer.adminlaundry.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.adminlaundry.CustomView.MyButton;
import removed.developer.adminlaundry.DataClass.ProgressOrders;
import removed.developer.adminlaundry.R;
import removed.developer.adminlaundry.models.DataOrder;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterProgressOrder extends RecyclerView.Adapter<AdapterProgressOrder.ViewHolder> {

    private List<DataOrder> progressOrdersArrayList;
    private Context context;

    public AdapterProgressOrder(List<DataOrder> progressOrdersArrayList, Context context) {
        this.progressOrdersArrayList = progressOrdersArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterProgressOrder.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_progress_order, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterProgressOrder.ViewHolder holder, int position) {
        DataOrder progressOrders = progressOrdersArrayList.get(position);

        holder.textViewNameCustomer.setText(progressOrders.getCustomerName());
        holder.textViewLocationCustomer.setText(progressOrders.getLocationName());
        holder.textViewPhone.setText(progressOrders.getCustomerPhone());
        holder.textViewWeightOrder.setText(progressOrders.getWeight().toString());
        holder.textViewPackage.setText(progressOrders.getPacketName());
        holder.textViewService.setText(progressOrders.getServiceName());
        holder.textViewAmountPrice.setText(progressOrders.getPrice().toString());
        holder.textViewDateFinish.setText(progressOrders.getFinishTime());
        if (progressOrders.getIsPaid() == 1){
            holder.textViewPaymentStatus.setText("Sudah Dibayar");
            holder.buttonDeliverry.setEnabled(true);
        }else{
            holder.textViewPaymentStatus.setText("Belum Dibayar");
            holder.buttonDeliverry.setEnabled(false);
        }

        holder.buttonDeliverry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.buttonDeliverry.setEnabled(false);
                ApiService service = ApiClient.getClient().create(ApiService.class);
                final Call<ResponseData> getData = service.updateOrder(
                        progressOrders.getId(),
                        "1",
                        "",
                        "",
                        "",
                        "",
                        ""
                );
                getData.enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                            holder.buttonDeliverry.setEnabled(false);
                        } else {
                            Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show();
                            holder.buttonDeliverry.setEnabled(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData> call, Throwable t) {
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                        holder.buttonDeliverry.setEnabled(true);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return progressOrdersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewNameCustomer, textViewLocationCustomer, textViewPhone, textViewWeightOrder,
                textViewPackage, textViewService, textViewAmountPrice, textViewDateFinish, textViewPaymentStatus;
        private MyButton buttonDeliverry;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNameCustomer = itemView.findViewById(R.id.textViewNameCustomer);
            textViewLocationCustomer = itemView.findViewById(R.id.textViewLocationCustomer);
            textViewPhone = itemView.findViewById(R.id.textViewPhone);
            textViewWeightOrder = itemView.findViewById(R.id.textViewWeightOrder);
            textViewPackage = itemView.findViewById(R.id.textViewPackage);
            textViewService = itemView.findViewById(R.id.textViewService);
            textViewAmountPrice = itemView.findViewById(R.id.textViewAmountPrice);
            textViewDateFinish = itemView.findViewById(R.id.textViewDateFinish);
            textViewPaymentStatus = itemView.findViewById(R.id.textViewPaymentStatus);
            buttonDeliverry = itemView.findViewById(R.id.buttonDeliverry);

        }
    }
}
