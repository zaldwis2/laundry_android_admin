package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import removed.developer.adminlaundry.Adapter.AdapterIncomingOrder;
import removed.developer.adminlaundry.DataClass.IncomingOrders;
import removed.developer.adminlaundry.databinding.ActivityIncomingOrderBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityIncomingOrder extends AppCompatActivity {

    ActivityIncomingOrderBinding binding;
    private AdapterIncomingOrder adapterIncomingOrder;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityIncomingOrderBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.toolbar.setTitle("Orderan Masuk");
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getIncomingOrders();
        swipeRefresh();
    }

    private void swipeRefresh(){
        binding.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getIncomingOrders();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.refresh.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

    private void getIncomingOrders(){
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getIncomingOrders(SharedPreferenceManager.getInstance(getApplicationContext()).getLaundryId());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    if (response.body().getDataIncomingOrder() != null){
                        linearLayoutManager = new LinearLayoutManager(ActivityIncomingOrder.this);
                        binding.recyclerView.setLayoutManager(linearLayoutManager);
                        adapterIncomingOrder = new AdapterIncomingOrder(response.body().getDataIncomingOrder(), getApplicationContext());
                        binding.recyclerView.setAdapter(adapterIncomingOrder);
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Failed Retrieve Data",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}