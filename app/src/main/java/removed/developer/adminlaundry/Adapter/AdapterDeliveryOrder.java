package removed.developer.adminlaundry.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.adminlaundry.CustomView.MyButton;
import removed.developer.adminlaundry.DataClass.DeliveryOrders;
import removed.developer.adminlaundry.DataClass.ProgressOrders;
import removed.developer.adminlaundry.R;
import removed.developer.adminlaundry.models.DataOrder;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterDeliveryOrder extends RecyclerView.Adapter<AdapterDeliveryOrder.ViewHolder> {

    private static final int REQUEST_PHONE_CALL = 1;
    private List<DataOrder> deliveryOrdersArrayList;
    private Context context;

    public AdapterDeliveryOrder(List<DataOrder> deliveryOrdersArrayList, Context context) {
        this.deliveryOrdersArrayList = deliveryOrdersArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterDeliveryOrder.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_delivery_order, parent, false);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterDeliveryOrder.ViewHolder holder, int position) {
        DataOrder deliveryOrders = deliveryOrdersArrayList.get(position);

        holder.textViewNameCustomer.setText(deliveryOrders.getCustomerName());
        holder.textViewLocationCustomer.setText(deliveryOrders.getLocationName());
        holder.textViewPhone.setText(deliveryOrders.getCustomerPhone());
        holder.textViewWeightOrder.setText(deliveryOrders.getWeight().toString());
        holder.textViewPackage.setText(deliveryOrders.getPacketName());
        holder.textViewService.setText(deliveryOrders.getServiceName());

        holder.buttonArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.buttonArrived.setEnabled(false);
                ApiService service = ApiClient.getClient().create(ApiService.class);
                final Call<ResponseData> getData = service.updateOrder(
                        deliveryOrders.getId(),
                        "0",
                        "",
                        "",
                        "1",
                        "",
                        ""
                );
                getData.enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                            holder.buttonArrived.setEnabled(false);
                        } else {
                            Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show();
                            holder.buttonArrived.setEnabled(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData> call, Throwable t) {
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                        holder.buttonArrived.setEnabled(true);
                    }
                });
            }
        });

        holder.buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + deliveryOrders.getCustomerPhone()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return deliveryOrdersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewNameCustomer, textViewLocationCustomer, textViewPhone, textViewWeightOrder, textViewPackage, textViewService;
        private MyButton buttonArrived, buttonCall;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNameCustomer = itemView.findViewById(R.id.textViewNameCustomer);
            textViewLocationCustomer = itemView.findViewById(R.id.textViewLocationCustomer);
            textViewPhone = itemView.findViewById(R.id.textViewPhone);
            textViewWeightOrder = itemView.findViewById(R.id.textViewWeightOrder);
            textViewPackage = itemView.findViewById(R.id.textViewPackage);
            textViewService = itemView.findViewById(R.id.textViewService);
            buttonArrived = itemView.findViewById(R.id.buttonArrived);
            buttonCall = itemView.findViewById(R.id.buttonCall);

        }
    }
}
