package removed.developer.adminlaundry.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.adminlaundry.CustomView.MyButton;
import removed.developer.adminlaundry.DataClass.IncomingOrders;
import removed.developer.adminlaundry.R;
import removed.developer.adminlaundry.models.DataOrder;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterIncomingOrder extends RecyclerView.Adapter<AdapterIncomingOrder.ViewHolder> {

    private List<DataOrder> incomingOrdersArrayList;
    private Context context;

    public AdapterIncomingOrder(List<DataOrder> incomingOrdersArrayList, Context context) {
        this.incomingOrdersArrayList = incomingOrdersArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterIncomingOrder.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_incoming_order, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterIncomingOrder.ViewHolder holder, int position) {
        final DataOrder incomingOrders = incomingOrdersArrayList.get(position);

        holder.textViewDatePickup.setText(incomingOrders.getPickupTime());
        holder.textViewNameCustomer.setText(incomingOrders.getCustomerName());
        holder.textViewPhone.setText(incomingOrders.getCustomerPhone());
        holder.textViewLocationCustomer.setText(incomingOrders.getLocationName());
        holder.textViewService.setText(incomingOrders.getServiceName());
        holder.textViewPackage.setText(incomingOrders.getPacketName());

        holder.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.buttonAccept.setEnabled(false);
                ApiService service = ApiClient.getClient().create(ApiService.class);
                final Call<ResponseData> getData = service.updateOrder(
                        incomingOrders.getId(),
                        "0",
                        "",
                        "1",
                        "",
                        "",
                        ""
                        );
                getData.enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                            holder.buttonAccept.setEnabled(false);
                        } else {
                            Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show();
                            holder.buttonAccept.setEnabled(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData> call, Throwable t) {
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                        holder.buttonAccept.setEnabled(true);
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return incomingOrdersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewDatePickup, textViewNameCustomer, textViewPhone, textViewLocationCustomer, textViewService, textViewPackage;
        private MyButton buttonAccept;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewDatePickup = itemView.findViewById(R.id.textViewDatePickup);
            textViewNameCustomer = itemView.findViewById(R.id.textViewNameCustomer);
            textViewPhone = itemView.findViewById(R.id.textViewPhone);
            textViewLocationCustomer = itemView.findViewById(R.id.textViewLocationCustomer);
            textViewService = itemView.findViewById(R.id.textViewService);
            textViewPackage = itemView.findViewById(R.id.textViewPackage);
            buttonAccept = itemView.findViewById(R.id.buttonAccept);
        }
    }
}
