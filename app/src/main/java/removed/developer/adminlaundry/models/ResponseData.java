package removed.developer.adminlaundry.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseData {
    @SerializedName("error")
    private Boolean error;

    @SerializedName("message")
    private String message;

    @SerializedName("balance")
    private Integer balance;

    @SerializedName("dataAdmin")
    @Expose
    private DataAdmin dataAdmin;

    @SerializedName("dataIncomingOrder")
    @Expose
    private List<DataOrder> dataIncomingOrder = null;

    @SerializedName("dataPickupOrder")
    @Expose
    private List<DataOrder> dataPickupOrder = null;

    @SerializedName("dataProcessOrder")
    @Expose
    private List<DataOrder> dataProcessOrder = null;

    @SerializedName("dataDeliverOrder")
    @Expose
    private List<DataOrder> dataDeliverOrder = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public DataAdmin getDataAdmin() {
        return dataAdmin;
    }

    public void setDataAdmin(DataAdmin dataAdmin) {
        this.dataAdmin = dataAdmin;
    }

    public List<DataOrder> getDataIncomingOrder() {
        return dataIncomingOrder;
    }

    public void setDataIncomingOrder(List<DataOrder> dataIncomingOrder) {
        this.dataIncomingOrder = dataIncomingOrder;
    }

    public List<DataOrder> getDataPickupOrder() {
        return dataPickupOrder;
    }

    public void setDataPickupOrder(List<DataOrder> dataPickupOrder) {
        this.dataPickupOrder = dataPickupOrder;
    }

    public List<DataOrder> getDataProcessOrder() {
        return dataProcessOrder;
    }

    public void setDataProcessOrder(List<DataOrder> dataProcessOrder) {
        this.dataProcessOrder = dataProcessOrder;
    }

    public List<DataOrder> getDataDeliverOrder() {
        return dataDeliverOrder;
    }

    public void setDataDeliverOrder(List<DataOrder> dataDeliverOrder) {
        this.dataDeliverOrder = dataDeliverOrder;
    }
}

