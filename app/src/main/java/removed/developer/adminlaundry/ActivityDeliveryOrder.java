package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import removed.developer.adminlaundry.Adapter.AdapterDeliveryOrder;
import removed.developer.adminlaundry.Adapter.AdapterIncomingOrder;
import removed.developer.adminlaundry.DataClass.DeliveryOrders;
import removed.developer.adminlaundry.databinding.ActivityDeliveryOrderBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDeliveryOrder extends AppCompatActivity {

    ActivityDeliveryOrderBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<DeliveryOrders> deliveryOrdersArrayList;
    private AdapterDeliveryOrder adapterDeliveryOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDeliveryOrderBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.toolbar.setTitle("Orderan Dianter");
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getDeliveryOrder();
        swipeRefresh();
    }

    private void swipeRefresh(){
        binding.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDeliveryOrder();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.refresh.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

    private void getDeliveryOrder() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getDeliverOrder(SharedPreferenceManager.getInstance(getApplicationContext()).getLaundryId());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataDeliverOrder() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityDeliveryOrder.this);
                        binding.recyclerView.setLayoutManager(linearLayoutManager);
                        adapterDeliveryOrder = new AdapterDeliveryOrder(response.body().getDataDeliverOrder(), ActivityDeliveryOrder.this);
                        binding.recyclerView.setAdapter(adapterDeliveryOrder);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed Retrieve Data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}