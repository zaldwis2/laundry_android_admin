package removed.developer.adminlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import removed.developer.adminlaundry.databinding.ActivityDashboardBinding;
import removed.developer.adminlaundry.models.ResponseData;
import removed.developer.adminlaundry.services.ApiClient;
import removed.developer.adminlaundry.services.ApiService;
import removed.developer.adminlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDashboard extends AppCompatActivity implements View.OnClickListener{

    ActivityDashboardBinding binding;
    private CardView cardViewIncomingOrder, cardViewPickupOrder, cardViewProgressOrder, cardViewDeliveryOrder,
            cardViewAccount, cardViewPassword, cardViewLogout;
    private TextView textViewNameAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //findView layout menu
        cardViewIncomingOrder = findViewById(R.id.cardViewIncomingOrder);
        cardViewPickupOrder = findViewById(R.id.cardViewPickupOrder);
        cardViewProgressOrder = findViewById(R.id.cardViewProgressOrder);
        cardViewDeliveryOrder = findViewById(R.id.cardViewDeliveryOrder);

        //findView layout profile
        cardViewAccount = findViewById(R.id.cardViewAccount);
        cardViewPassword = findViewById(R.id.cardViewPassword);
        cardViewLogout = findViewById(R.id.cardViewLogout);
        textViewNameAdmin = findViewById(R.id.textViewNameAdmin);

        cardViewIncomingOrder.setOnClickListener(this);
        cardViewPickupOrder.setOnClickListener(this);
        cardViewProgressOrder.setOnClickListener(this);
        cardViewDeliveryOrder.setOnClickListener(this);
        cardViewAccount.setOnClickListener(this);
        cardViewPassword.setOnClickListener(this);
        cardViewLogout.setOnClickListener(this);

        dataAdminAPI(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cardViewIncomingOrder:
                Intent intentIncomingOrder = new Intent(ActivityDashboard.this, ActivityIncomingOrder.class);
                startActivity(intentIncomingOrder);
                break;

            case R.id.cardViewPickupOrder:
                Intent intentPickupOrder = new Intent(ActivityDashboard.this, ActivityPickupOrder.class);
                startActivity(intentPickupOrder);
                break;

            case R.id.cardViewProgressOrder:
                Intent intentProgressOrder = new Intent(ActivityDashboard.this, ActivityProgressOrder.class);
                startActivity(intentProgressOrder);
                break;

            case R.id.cardViewDeliveryOrder:
                Intent intentDeliverOrder = new Intent(ActivityDashboard.this, ActivityDeliveryOrder.class);
                startActivity(intentDeliverOrder);
                break;

            case R.id.cardViewAccount:
                Intent intetAccount = new Intent(ActivityDashboard.this, ActivityChangeProfile.class);
                startActivity(intetAccount);
                break;

            case R.id.cardViewPassword:
                Intent intentPassword = new Intent(ActivityDashboard.this, ActivityChangePassword.class);
                startActivity(intentPassword);
                break;

            case R.id.cardViewLogout:
                logoutMethod();
                break;
        }
    }

    private void dataAdminAPI(String id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getDataAdmin(id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    binding.textView2.setText("Halo, "+response.body().getDataAdmin().getName());
                    binding.textViewLocationBranch.setText("Cabang : "+response.body().getDataAdmin().getLaundryName());
                    binding.textViewBalance.setText(response.body().getDataAdmin().getBalance().toString());
                    textViewNameAdmin.setText(response.body().getDataAdmin().getName());
                }else{
                    Toast.makeText(getApplicationContext(),"Failed Retrieve Data",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void logoutMethod(){
        SharedPreferenceManager.getInstance(ActivityDashboard.this).setUserId("");
        SharedPreferenceManager.getInstance(ActivityDashboard.this).setLaundryId("");
        Intent intent = new Intent(ActivityDashboard.this, ActivityLogin.class);
        startActivity(intent);
        finishAffinity();
    }
}